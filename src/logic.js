const allowedCharacters =
  "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
const lowercase = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
const uppercase = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

export function createDataTableRows(currentPhrase){
  return [...Array(33)].map((e, i) => {
    return {
      number: i,
      text: encrypt(currentPhrase, i),
    };
  });
}

function encrypt(phrase, shift) {
  const newPhrase = [];
  for (let i = 0; i < phrase.length; i++) {
    const currentCharacter = phrase[i];

    if (lowercase.includes(currentCharacter)) {
      const oldIndex = lowercase.indexOf(currentCharacter);
      const newIndex = (oldIndex + shift) % lowercase.length;
      const newCharacter = lowercase[newIndex];
      newPhrase.push(newCharacter);
    } else if (uppercase.includes(currentCharacter)) {
      const oldIndex = uppercase.indexOf(currentCharacter);
      const newIndex = (oldIndex + shift) % uppercase.length;
      const newCharacter = uppercase[newIndex];
      newPhrase.push(newCharacter);
    } else if (currentCharacter === " ") {
      newPhrase.push(currentCharacter);
    }
  }

  return newPhrase.join("");
}

export function onSourceTextChanged(event, setCurrentPhrase) {
  const newPhrase = event.target.value;

  for (let i = 0; i < newPhrase.length; i++) {
    const currentCharacter = newPhrase[i];
    if (currentCharacter === " ") continue;
    if (!allowedCharacters.includes(currentCharacter)) {
      return;
    }
  }

  setCurrentPhrase(newPhrase);
}
