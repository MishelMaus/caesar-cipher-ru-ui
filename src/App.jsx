import { useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";

import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

import { createDataTableRows, onSourceTextChanged } from "./logic";

export default function App() {
  const [currentPhrase, setCurrentPhrase] = useState("");

  const rows = createDataTableRows(currentPhrase);

  const onChanged = event => onSourceTextChanged(event, setCurrentPhrase);

  return (
    <Box>
      <Typography variant="h1">Шифр Цезаря</Typography>
      <Grid container={true}>
        <Grid item={true} xs={12}>
          <TextField
            label="Исходный текст"
            value={currentPhrase}
            onChange={onChanged}
            fullWidth={true}
          />
        </Grid>

        <Grid item={true} xs={12}>
          <TableContainer component={Paper}>
            <Table
              sx={{
                minWidth: 650
              }}
              size="small"
            >
              <TableHead>
                <TableRow>
                  <TableCell align="right">Сдвиг</TableCell>
                  <TableCell align="left">Текст</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map(row => (
                  <TableRow
                    key={row.number}
                    sx={{
                      "&:last-child td, &:last-child th": {
                        border: 0
                      }
                    }}
                  >
                    <TableCell
                      width="5%"
                      component="th"
                      scope="row"
                      align="right"
                    >
                      {row.number}
                    </TableCell>
                    <TableCell width="95%" align="left">
                      {row.text}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
    </Box>
  );
}
