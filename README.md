# Caesar cipher

This project is a React-based UI for encrypting and decrypting Russian texts with Caesar cipher

## Usage

Launch it with `npm start` and navigate to `localhost:3000`
